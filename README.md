# Private Module 起手式

> 先附教學連結 [教學連結](https://medium.com/@BrodaNoel/how-to-create-a-react-component-and-publish-it-in-npm-668ad7d363ce)

## 目的

* 有多個不同的 `project`
* 會用到相同的 `module`
* 不想貢獻到 `NPM` (如果想放上去也可以) :smile_cat:

## 步驟

1. 將要共用的 module 抽出來
2. 創建一個新的 `repository`
3. npm init 新的 project
4. 在 package.json 加入需要的 dependencies 或參考此 `project` 的
5. 把 webpack.js, .babelrc 抄進來
6. 將 refactor 的 module 放入 src/index.js
7. yarn build
8. comit + push
9. 在您需要的 project 中引用此 project (非ssh 的 git)

引用如下
```
yarn add https://gitlab.com/yuhsiang.l/private_module.git
```

## 如何更新

1. 更改 src/index.js
2. yarn build  # 重新 compile
3. comit + push
4. 回到你的 project 跑更新

如下
```
yarn upgrade private_module
```

